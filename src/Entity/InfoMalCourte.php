<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InfoMalCourteRepository")
 */
class InfoMalCourte
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Maladies", inversedBy="infoMalCourtes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $maladies;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $info;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\QuestionCourte", mappedBy="infoCourte", orphanRemoval=true)
     */
    private $questionCourtes;

    public function __construct()
    {
        $this->questionCourtes = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMaladies(): ?Maladies
    {
        return $this->maladies;
    }

    public function setMaladies(?Maladies $maladies): self
    {
        $this->maladies = $maladies;

        return $this;
    }

    public function getInfo(): ?string
    {
        return $this->info;
    }

    public function setInfo(string $info): self
    {
        $this->info = $info;

        return $this;
    }

    /**
     * @return Collection|QuestionCourte[]
     */
    public function getQuestionCourtes(): Collection
    {
        return $this->questionCourtes;
    }

    public function addQuestionCourte(QuestionCourte $questionCourte): self
    {
        if (!$this->questionCourtes->contains($questionCourte)) {
            $this->questionCourtes[] = $questionCourte;
            $questionCourte->setInfoCourte($this);
        }

        return $this;
    }

    public function removeQuestionCourte(QuestionCourte $questionCourte): self
    {
        if ($this->questionCourtes->contains($questionCourte)) {
            $this->questionCourtes->removeElement($questionCourte);
            // set the owning side to null (unless already changed)
            if ($questionCourte->getInfoCourte() === $this) {
                $questionCourte->setInfoCourte(null);
            }
        }

        return $this;
    }
}
