<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\QuestionLongRepository")
 */
class QuestionLong
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Madestination", inversedBy="questionLongs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $destination;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\InfoMalLong", inversedBy="questionLongs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $info_long;

    /**
     * @ORM\Column(type="text")
     */
    private $question;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $personnalisation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDestination(): ?Madestination
    {
        return $this->destination;
    }

    public function setDestination(?Madestination $destination): self
    {
        $this->destination = $destination;

        return $this;
    }

    public function getInfoLong(): ?InfoMalLong
    {
        return $this->info_long;
    }

    public function setInfoLong(?InfoMalLong $info_long): self
    {
        $this->info_long = $info_long;

        return $this;
    }

    public function getQuestion(): ?string
    {
        return $this->question;
    }

    public function setQuestion(string $question): self
    {
        $this->question = $question;

        return $this;
    }

    public function getPersonnalisation(): ?string
    {
        return $this->personnalisation;
    }

    public function setPersonnalisation(string $personnalisation): self
    {
        $this->personnalisation = $personnalisation;

        return $this;
    }
}
