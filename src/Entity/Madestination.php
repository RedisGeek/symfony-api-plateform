<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\MadestinationRepository")
 */
class Madestination
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom_pays;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\QuestionCourte", mappedBy="destination")
     */
    private $questionCourtes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\QuestionLong", mappedBy="destination", orphanRemoval=true)
     */
    private $questionLongs;

    public function __construct()
    {
        $this->questionCourtes = new ArrayCollection();
        $this->questionLongs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomPays(): ?string
    {
        return $this->nom_pays;
    }

    public function setNomPays(string $nom_pays): self
    {
        $this->nom_pays = $nom_pays;

        return $this;
    }

    /**
     * @return Collection|QuestionCourte[]
     */
    public function getQuestionCourtes(): Collection
    {
        return $this->questionCourtes;
    }

    public function addQuestionCourte(QuestionCourte $questionCourte): self
    {
        if (!$this->questionCourtes->contains($questionCourte)) {
            $this->questionCourtes[] = $questionCourte;
            $questionCourte->setDestination($this);
        }

        return $this;
    }

    public function removeQuestionCourte(QuestionCourte $questionCourte): self
    {
        if ($this->questionCourtes->contains($questionCourte)) {
            $this->questionCourtes->removeElement($questionCourte);
            // set the owning side to null (unless already changed)
            if ($questionCourte->getDestination() === $this) {
                $questionCourte->setDestination(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|QuestionLong[]
     */
    public function getQuestionLongs(): Collection
    {
        return $this->questionLongs;
    }

    public function addQuestionLong(QuestionLong $questionLong): self
    {
        if (!$this->questionLongs->contains($questionLong)) {
            $this->questionLongs[] = $questionLong;
            $questionLong->setDestination($this);
        }

        return $this;
    }

    public function removeQuestionLong(QuestionLong $questionLong): self
    {
        if ($this->questionLongs->contains($questionLong)) {
            $this->questionLongs->removeElement($questionLong);
            // set the owning side to null (unless already changed)
            if ($questionLong->getDestination() === $this) {
                $questionLong->setDestination(null);
            }
        }

        return $this;
    }

}
