<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MaladiesRepository")
 */
class Maladies
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\InfoMalCourte", mappedBy="maladies")
     */
    private $infoMalCourtes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\InfoMalLong", mappedBy="maladies")
     */
    private $infoMalLongs;

    public function __construct()
    {
        $this->infoMalCourtes = new ArrayCollection();
        $this->infoMalLongs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection|InfoMalCourte[]
     */
    public function getInfoMalCourtes(): Collection
    {
        return $this->infoMalCourtes;
    }

    public function addInfoMalCourte(InfoMalCourte $infoMalCourte): self
    {
        if (!$this->infoMalCourtes->contains($infoMalCourte)) {
            $this->infoMalCourtes[] = $infoMalCourte;
            $infoMalCourte->setMaladies($this);
        }

        return $this;
    }

    public function removeInfoMalCourte(InfoMalCourte $infoMalCourte): self
    {
        if ($this->infoMalCourtes->contains($infoMalCourte)) {
            $this->infoMalCourtes->removeElement($infoMalCourte);
            // set the owning side to null (unless already changed)
            if ($infoMalCourte->getMaladies() === $this) {
                $infoMalCourte->setMaladies(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|InfoMalLong[]
     */
    public function getInfoMalLongs(): Collection
    {
        return $this->infoMalLongs;
    }

    public function addInfoMalLong(InfoMalLong $infoMalLong): self
    {
        if (!$this->infoMalLongs->contains($infoMalLong)) {
            $this->infoMalLongs[] = $infoMalLong;
            $infoMalLong->setMaladies($this);
        }

        return $this;
    }

    public function removeInfoMalLong(InfoMalLong $infoMalLong): self
    {
        if ($this->infoMalLongs->contains($infoMalLong)) {
            $this->infoMalLongs->removeElement($infoMalLong);
            // set the owning side to null (unless already changed)
            if ($infoMalLong->getMaladies() === $this) {
                $infoMalLong->setMaladies(null);
            }
        }

        return $this;
    }
}
