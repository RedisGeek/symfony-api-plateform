<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InfoMalLongRepository")
 */
class InfoMalLong
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Maladies", inversedBy="infoMalLongs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $maladies;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $niv_risq1;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $niv_risq2;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $periode_trans1;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $periode_trans2;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\QuestionLong", mappedBy="info_long", orphanRemoval=true)
     */
    private $questionLongs;

    public function __construct()
    {
        $this->questionLongs = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMaladies(): ?Maladies
    {
        return $this->maladies;
    }

    public function setMaladies(?Maladies $maladies): self
    {
        $this->maladies = $maladies;

        return $this;
    }

    public function getNivRisq1(): ?string
    {
        return $this->niv_risq1;
    }

    public function setNivRisq1(string $niv_risq1): self
    {
        $this->niv_risq1 = $niv_risq1;

        return $this;
    }

    public function getNivRisq2(): ?string
    {
        return $this->niv_risq2;
    }

    public function setNivRisq2(string $niv_risq2): self
    {
        $this->niv_risq2 = $niv_risq2;

        return $this;
    }

    public function getPeriodeTrans1(): ?string
    {
        return $this->periode_trans1;
    }

    public function setPeriodeTrans1(string $periode_trans1): self
    {
        $this->periode_trans1 = $periode_trans1;

        return $this;
    }

    public function getPeriodeTrans2(): ?string
    {
        return $this->periode_trans2;
    }

    public function setPeriodeTrans2(string $periode_trans2): self
    {
        $this->periode_trans2 = $periode_trans2;

        return $this;
    }

    /**
     * @return Collection|QuestionLong[]
     */
    public function getQuestionLongs(): Collection
    {
        return $this->questionLongs;
    }

    public function addQuestionLong(QuestionLong $questionLong): self
    {
        if (!$this->questionLongs->contains($questionLong)) {
            $this->questionLongs[] = $questionLong;
            $questionLong->setInfoLong($this);
        }

        return $this;
    }

    public function removeQuestionLong(QuestionLong $questionLong): self
    {
        if ($this->questionLongs->contains($questionLong)) {
            $this->questionLongs->removeElement($questionLong);
            // set the owning side to null (unless already changed)
            if ($questionLong->getInfoLong() === $this) {
                $questionLong->setInfoLong(null);
            }
        }

        return $this;
    }
}
