<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190424114945 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE monvoyage ADD utilisateur_id INT DEFAULT NULL, CHANGE destination_id destination_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE monvoyage ADD CONSTRAINT FK_1738DF04FB88E14F FOREIGN KEY (utilisateur_id) REFERENCES utilisateur (id)');
        $this->addSql('CREATE INDEX IDX_1738DF04FB88E14F ON monvoyage (utilisateur_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE monvoyage DROP FOREIGN KEY FK_1738DF04FB88E14F');
        $this->addSql('DROP INDEX IDX_1738DF04FB88E14F ON monvoyage');
        $this->addSql('ALTER TABLE monvoyage DROP utilisateur_id, CHANGE destination_id destination_id INT DEFAULT NULL');
    }
}
