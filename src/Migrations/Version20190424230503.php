<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190424230503 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE question_courte (id INT AUTO_INCREMENT NOT NULL, destination_id INT NOT NULL, info_courte_id INT NOT NULL, question LONGTEXT NOT NULL, INDEX IDX_5E82D676816C6140 (destination_id), INDEX IDX_5E82D676464CA8BF (info_courte_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE question_courte ADD CONSTRAINT FK_5E82D676816C6140 FOREIGN KEY (destination_id) REFERENCES madestination (id)');
        $this->addSql('ALTER TABLE question_courte ADD CONSTRAINT FK_5E82D676464CA8BF FOREIGN KEY (info_courte_id) REFERENCES info_mal_courte (id)');
        $this->addSql('ALTER TABLE monvoyage CHANGE destination_id destination_id INT DEFAULT NULL, CHANGE utilisateur_id utilisateur_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE info_mal_long DROP FOREIGN KEY FK_CB9EB777816C6140');
        $this->addSql('DROP INDEX IDX_CB9EB777816C6140 ON info_mal_long');
        $this->addSql('ALTER TABLE info_mal_long DROP destination_id');
        $this->addSql('ALTER TABLE info_mal_courte DROP FOREIGN KEY FK_6E8A3A8A816C6140');
        $this->addSql('DROP INDEX IDX_6E8A3A8A816C6140 ON info_mal_courte');
        $this->addSql('ALTER TABLE info_mal_courte DROP destination_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE question_courte');
        $this->addSql('ALTER TABLE info_mal_courte ADD destination_id INT NOT NULL');
        $this->addSql('ALTER TABLE info_mal_courte ADD CONSTRAINT FK_6E8A3A8A816C6140 FOREIGN KEY (destination_id) REFERENCES madestination (id)');
        $this->addSql('CREATE INDEX IDX_6E8A3A8A816C6140 ON info_mal_courte (destination_id)');
        $this->addSql('ALTER TABLE info_mal_long ADD destination_id INT NOT NULL');
        $this->addSql('ALTER TABLE info_mal_long ADD CONSTRAINT FK_CB9EB777816C6140 FOREIGN KEY (destination_id) REFERENCES madestination (id)');
        $this->addSql('CREATE INDEX IDX_CB9EB777816C6140 ON info_mal_long (destination_id)');
        $this->addSql('ALTER TABLE monvoyage CHANGE destination_id destination_id INT DEFAULT NULL, CHANGE utilisateur_id utilisateur_id INT DEFAULT NULL');
    }
}
