<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190425000338 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE question_long (id INT AUTO_INCREMENT NOT NULL, destination_id INT NOT NULL, info_long_id INT NOT NULL, question LONGTEXT NOT NULL, INDEX IDX_65794487816C6140 (destination_id), INDEX IDX_65794487B3EE774A (info_long_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE question_long ADD CONSTRAINT FK_65794487816C6140 FOREIGN KEY (destination_id) REFERENCES madestination (id)');
        $this->addSql('ALTER TABLE question_long ADD CONSTRAINT FK_65794487B3EE774A FOREIGN KEY (info_long_id) REFERENCES info_mal_long (id)');
        $this->addSql('ALTER TABLE monvoyage CHANGE destination_id destination_id INT DEFAULT NULL, CHANGE utilisateur_id utilisateur_id INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE question_long');
        $this->addSql('ALTER TABLE monvoyage CHANGE destination_id destination_id INT DEFAULT NULL, CHANGE utilisateur_id utilisateur_id INT DEFAULT NULL');
    }
}
