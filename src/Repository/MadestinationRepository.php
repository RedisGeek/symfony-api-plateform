<?php

namespace App\Repository;

use App\Entity\Madestination;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Madestination|null find($id, $lockMode = null, $lockVersion = null)
 * @method Madestination|null findOneBy(array $criteria, array $orderBy = null)
 * @method Madestination[]    findAll()
 * @method Madestination[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MadestinationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Madestination::class);
    }

    // /**
    //  * @return Madestination[] Returns an array of Madestination objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Madestination
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
