<?php

namespace App\Repository;

use App\Entity\QuestionCourte;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method QuestionCourte|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuestionCourte|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuestionCourte[]    findAll()
 * @method QuestionCourte[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuestionCourteRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, QuestionCourte::class);
    }

    // /**
    //  * @return QuestionCourte[] Returns an array of QuestionCourte objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?QuestionCourte
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
