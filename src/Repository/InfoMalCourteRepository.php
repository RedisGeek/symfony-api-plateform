<?php

namespace App\Repository;

use App\Entity\InfoMalCourte;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method InfoMalCourte|null find($id, $lockMode = null, $lockVersion = null)
 * @method InfoMalCourte|null findOneBy(array $criteria, array $orderBy = null)
 * @method InfoMalCourte[]    findAll()
 * @method InfoMalCourte[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InfoMalCourteRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, InfoMalCourte::class);
    }

    // /**
    //  * @return InfoMalCourte[] Returns an array of InfoMalCourte objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InfoMalCourte
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
