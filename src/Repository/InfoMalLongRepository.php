<?php

namespace App\Repository;

use App\Entity\InfoMalLong;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method InfoMalLong|null find($id, $lockMode = null, $lockVersion = null)
 * @method InfoMalLong|null findOneBy(array $criteria, array $orderBy = null)
 * @method InfoMalLong[]    findAll()
 * @method InfoMalLong[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InfoMalLongRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, InfoMalLong::class);
    }

    // /**
    //  * @return InfoMalLong[] Returns an array of InfoMalLong objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?InfoMalLong
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
