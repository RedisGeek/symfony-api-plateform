<?php

namespace App\Repository;

use App\Entity\QuestionLong;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method QuestionLong|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuestionLong|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuestionLong[]    findAll()
 * @method QuestionLong[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuestionLongRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, QuestionLong::class);
    }

    // /**
    //  * @return QuestionLong[] Returns an array of QuestionLong objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?QuestionLong
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
