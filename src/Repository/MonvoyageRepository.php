<?php

namespace App\Repository;

use App\Entity\Monvoyage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Monvoyage|null find($id, $lockMode = null, $lockVersion = null)
 * @method Monvoyage|null findOneBy(array $criteria, array $orderBy = null)
 * @method Monvoyage[]    findAll()
 * @method Monvoyage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MonvoyageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Monvoyage::class);
    }

    // /**
    //  * @return Monvoyage[] Returns an array of Monvoyage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Monvoyage
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
