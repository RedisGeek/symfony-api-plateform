<?php

namespace App\Controller;

use App\Entity\Monvoyage;
use App\Form\MonvoyageType;
use App\Repository\MonvoyageRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/voyage")
 */
class MonvoyageController extends AbstractController
{
    /**
     * @Route("/", name="monvoyage_index", methods={"GET"})
     */
    public function index(MonvoyageRepository $monvoyageRepository): Response
    {

        return $this->render('monvoyage/index.html.twig', [
            'monvoyages' => $monvoyageRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="monvoyage_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $monvoyage = new Monvoyage();
        $form = $this->createForm(MonvoyageType::class, $monvoyage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($monvoyage);
            $entityManager->flush();

            return $this->redirectToRoute('monvoyage_index');
        }

        return $this->render('monvoyage/new.html.twig', [
            'monvoyage' => $monvoyage,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="monvoyage_show", methods={"GET"})
     */
    public function show(Monvoyage $monvoyage): Response
    {
        return $this->render('monvoyage/show.html.twig', [
            'monvoyage' => $monvoyage,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="monvoyage_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Monvoyage $monvoyage): Response
    {
        $form = $this->createForm(MonvoyageType::class, $monvoyage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('monvoyage_index', [
                'id' => $monvoyage->getId(),
            ]);
        }

        return $this->render('monvoyage/edit.html.twig', [
            'monvoyage' => $monvoyage,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="monvoyage_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Monvoyage $monvoyage): Response
    {
        if ($this->isCsrfTokenValid('delete'.$monvoyage->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($monvoyage);
            $entityManager->flush();
        }

        return $this->redirectToRoute('monvoyage_index');
    }
}
