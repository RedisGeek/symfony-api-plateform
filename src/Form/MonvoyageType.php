<?php

namespace App\Form;

use App\Entity\Monvoyage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Madestination;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class MonvoyageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date_depart', DateType::class,[
                'widget' => 'single_text'
            ])
            ->add('date_arrive', DateType::class,[
                'widget' => 'single_text'
            ])
            ->add('destination',EntityType::class,[
                    'class'         => Madestination::class,
                    'choice_label'  => 'nom_pays'
                ])
            //->add('utilisateur')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Monvoyage::class,
        ]);
    }
}
