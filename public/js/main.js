const user = document.getElementById('user');

if(user){
    user.addEventListener('click', e =>{
   
        if(e.target.className === 'btn btn-danger delete-user'){
            if(confirm("Voullez vous vraiment supprimer?")){
                const id = e.target.getAttribute('data-id')
                fetch(`http://laab4036.odns.fr/travelkit-app/public/index.php/utilisateur/delete/${id}`,{
                    method: 'DELETE'
                }).then(res => window.location.reload());
            }
        }
    })
}

